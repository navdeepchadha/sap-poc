import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Logging } from 'app/common/utils/logging';
import 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'app';
  
  logging = new Logging();

  constructor(private router:Router){

    // this.router.events.filter(event => event instanceof NavigationEnd).subscribe(d=>{
    //   //this.logging.log(this,d);
    //   //this.logging.log(this, this.router.routerState.snapshot['_root'].children[0].value._routeConfig);
    //   for(let item of this.router.routerState.snapshot['_root'].children){
    //     //this.logging.log(this,item.value._routeConfig);  //printing every route segments
    //     if(item.value._routeConfig.outlet){
    //       this.logging.log(this,item.value._routeConfig);  //filterd only when outlet is defined
    //     }
    //   }
      
    // })

  }

}
