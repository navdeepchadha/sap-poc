import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from 'app/common/components/page-not-found/page-not-found.component';
import { LoginComponent } from './modules/login/login.component';
import { AuthGuard } from 'app/auth-guard.service';
import { RiskOccurrencesModule } from 'app/modules/risk-occurrences/risk-occurrences.module';
import { VMNodeAccessComponent } from 'app/modules/vm-node/vm-node-access/vm-node-access.component';
import { RiskOccurrencesComponent } from 'app/modules/risk-occurrences/risk-occurrences.component';
import { SupportTicketAccessComponent } from 'app/modules/support-ticket-access/support-ticket-access.component';
import { CarouselComponent } from './modules/carousel/carousel.component';
import { TableComponent } from './modules/table/table.component';
import { TableDialog } from './modules/table/tabledialog';
import { SliderComponent } from 'app/common/components/slider/slider.component';

const appRoutes: Routes = [
  { path: 'dashboard', loadChildren: 'app/modules/dashboard/dashboard.module#DashboardModule', canLoad: [AuthGuard] },
  { path: 'risk-by-union', loadChildren: 'app/modules/risk-by-union/risk.module#RiskModule', canLoad: [AuthGuard] },
  // named router implemented from http://onehungrymind.com/named-router-outlets-in-angular-2/ 
  { path: ':riskOccurrencesId', component: RiskOccurrencesComponent, outlet: 'risk-occurrences' },
  { path: ':vmNodeId', component: VMNodeAccessComponent, outlet: 'vm-node' },
  { path: ':supportTicketId', component: SupportTicketAccessComponent, outlet: 'support-ticket' },
  { path: 'login', component: LoginComponent },
  { path: '1', component:SliderComponent},
  { path: 'carousel', component: CarouselComponent },
  { path: 'table', component: TableComponent },  
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  //{ path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers: [
    AuthGuard
  ]
})
export class AppRoutingModule { }



function newFunction() {
    SliderComponent;
}
/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/