import { Component, OnInit } from '@angular/core';


declare var google;

@Component({
    selector: 'line-chart',
    template: '<div id="Linechart_div"></div>'
})

export class LineChartComponent implements OnInit {

    drawLineChart() {

        var presentDate = new Date();
        var currentYear = presentDate.getFullYear();


        var data = new google.visualization.DataTable();
        data.addColumn('date', 'Month');
        data.addColumn('number', 'Current');
        data.addColumn('number', 'Projected');

        data.addRows([
            [new Date(currentYear, 0), 0, 0], [new Date(currentYear, 1), 1, 10], [new Date(currentYear, 2), 2, 23],
            [new Date(currentYear, 3), 3, 17], [new Date(currentYear, 4), 4, 18], [new Date(currentYear, 5), 5, 9],
            [new Date(currentYear, 6), 6, 11], [new Date(currentYear, 7), 10.3, 16.6],
            [new Date(currentYear, 8), 7.4, 13.3],
            [new Date(currentYear, 9), 4.4, 9.9],
            [new Date(currentYear, 10), 1.1, 6.6],
            [new Date(currentYear, 11), 2, 4.5]
        ]);


        // var formatter = new google.visualization.NumberFormat({
        //   fractionDigits: 2,
        //   prefix: 'R$:'
        // });
        // formatter.format(data, 1);
        var dateFormatter = new google.visualization.NumberFormat({
            pattern: 'MMM yyyy'
        });
        dateFormatter.format(data, 0);

        var options = {
            hAxis: {
                // title: 'example title',
                slantedText: false,
                slantedTextAngle: 45,
                textStyle: {
                    fontSize: 10
                },
                format: 'MMM'
            },
            vAxis: {
                // title: 'Popularity'
            },
            series: {
                0: { color: '#3e708b', pointsVisible: true },
                1: { curveType: 'function', pointsVisible: true, lineDashStyle: [4, 4] }
            },
            legend: { position: 'bottom', textStyle: { color: 'black', fontSize: 9 } }
        };

        var chart = new google.visualization.LineChart(document.getElementById('Linechart_div'));

        chart.draw(data, options);
    }

    ngOnInit() {
        google.charts.load('current', { 'packages': ['line'] });
        google.charts.setOnLoadCallback(this.drawLineChart);
    }

}