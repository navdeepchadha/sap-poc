import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'


declare var google;

@Component({
    selector: 'geo-chart',
    template: '<div id="geochart-colors"></div>'
})

export class GeoChartComponent implements OnInit {
    constructor(private route: ActivatedRoute, private router: Router) {
    }

    drawRegionsMap(s) {

        var dataRows = [
            ['Country', 'Risk'],
            ['Germany', 36], ['Poland', 8], ['France', 6], ['Italy', 24], ['Netherlands', 6], ['United Kingdom', 24]

        ];


        // extract column index 1 for color values
        var colorValues = dataRows.map(function (val) {
            return val.slice(-1);
        });
        // remove column label
        colorValues.splice(0, 1);
        // sort ascending
        colorValues.sort(function (a: any, b: any) { return a - b });

        // build color names red <= -10 > yellow <= 10 > green
        var colorNames = [];

        colorValues.forEach((value: any) => {
            // for (var i = 0; i < colorValues.length; i++) {
            if (value <= 15) {
                colorNames.push('#6CC417');
            } else if ((value > 15) && (value <= 30)) {
                colorNames.push('#3090C7');
            } else {
                colorNames.push('#DC381F');
            }
        });

        var data = google.visualization.arrayToDataTable(dataRows);
        var options = {
            region: '150', // Europe
            colorAxis: {
                colors: colorNames,
                values: colorValues
            },
            backgroundColor: '#81d4fa',
            datalessRegionColor: '#ffffff',
            defaultColor: '#f5f5f5',
            legend: 'none',
            magnifyingGlass: { enable: true, zoomFactor: 7.5 },
            keepAspectRatio: true

        };




        var geoChart = new google.visualization.GeoChart(document.getElementById('geochart-colors'));
        geoChart.draw(data, options);
        google.visualization.events.addListener(geoChart, 'select', (obj) => {
            var selectedItem = geoChart.getSelection();
            //console.log(selectedItem);
            s.mapclicked(selectedItem);
        });



    };

    ngOnInit() {



        google.charts.load('current', {
            'packages': ['geochart'],
            // Note: you will need to get a mapsApiKey for your project.
            // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
            'mapsApiKey': 'AIzaSyBa9Aj01s0c08PJC2jtGOSGeyJSSMJNbno'
        });
        google.charts.setOnLoadCallback(() => this.drawRegionsMap(this));
    }

    mapclicked(obj) {
        let unionId = obj[0].row;
        console.log(obj)
        this.router.navigate(['/risk-by-union', unionId, 'data-movement']);
    }

}