import { Component, OnInit, Input, OnChanges } from '@angular/core';


declare var google;

@Component({
    selector: 'scatter-chart',
    template: '<div id="chart_div"></div>'
})

export class ScatterChartComponent implements OnInit, OnChanges{

    @Input() chartData = [
        // ['8pm', 0.00],
        [0, 20, 67], [1, 17, 88], [2, 2, 77],
        [3, 5, 93], [4, 4, 85], [5, 35, 91],
        [6, 15, 71], [7, 27, 78], [8, 8, 93],
        [9, 9, 80], [10, 10, 82], [11, 10, 75],
        [12, 17, 80], [13, 3, 90], [14, 21, 72],
        // ['6am', 50.00]
    ]

    drawScatterChart(chartdata) {

        var data = new google.visualization.DataTable();
        data.addColumn('number', 'Time');
        data.addColumn('number', 'Low');
        data.addColumn('number', 'High');

        data.addRows(chartdata);

        var options = {
            // title: 'Age vs. Weight comparison',
            // hAxis: { title: 'Age', minValue: 0, maxValue: 15 },
            vAxis: { title: 'Risks' },
            legend: { position: 'bottom', textStyle: { color: 'black', fontSize: 9 } },
            colors: ['#087037', 'red'],

        };

        var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));

        chart.draw(data, options);
    }

    googleChartLoaded = false;
    ngOnInit() {
        google.charts.load('current', { 'packages': ['corechart'] });
        google.charts.setOnLoadCallback(()=>{
            this.googleChartLoaded = true;
            this.drawScatterChart(this.chartData)
        });
    }

    ngOnChanges(){
        if(this.googleChartLoaded){
            this.drawScatterChart(this.chartData);
        }
    }

}