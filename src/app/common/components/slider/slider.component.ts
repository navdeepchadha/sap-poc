import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }

  scatterChartData = [
    [0, 20, 67], [1, 17, 88], [2, 2, 77],
    [3, 5, 93], [4, 4, 85], [5, 35, 91],
    [6, 15, 71], [7, 27, 78], [8, 8, 93],
    [9, 9, 80], [10, 10, 82], [11, 10, 75],
    [12, 17, 80], [13, 3, 90], [14, 21, 72],
  ]
   openNav() {
    document.getElementById("slider1").style.width = "800px";
    document.getElementById("main").style.marginRight = "800px";
    document.body.style.backgroundColor = "rgba(0,0,0,0)";
    this.scatterChartData = this.scatterChartData.slice(this.scatterChartData.length-4, this.scatterChartData.length)
}

 closeNav() {
    document.getElementById("slider1").style.width = "0";
    document.getElementById("main").style.marginRight= "0";
    document.body.style.backgroundColor = "white";
}
 openNavi() {
    
    document.getElementById("slider2").style.width = "400px";
    document.getElementById("sidenav-wrapper").style.marginRight = "390px";
    document.body.style.backgroundColor = "rgba(0,0,0,0)";
}
 closeNavi() {
    document.getElementById("nav1").style.width = "0";
    document.getElementById("slider1").style.marginRight= "0";
    document.body.style.backgroundColor = "white";
}
 expand(){
  document.getElementById("slider2").style.width="100%";

}
 currentwidth(e, s){
console.log('-----',e,s);
}

}
