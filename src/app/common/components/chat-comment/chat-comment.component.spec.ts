import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatCommentComponent } from './chat-comment.component';

describe('ChatCommentComponent', () => {
  let component: ChatCommentComponent;
  let fixture: ComponentFixture<ChatCommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatCommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
