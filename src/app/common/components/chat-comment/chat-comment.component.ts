import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-chat-comment',
  templateUrl: './chat-comment.component.html',
  styleUrls: ['./chat-comment.component.css']
})
export class ChatCommentComponent implements OnInit {

  @Output() saved = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  save(){
    this.saved.emit();
  }
}
