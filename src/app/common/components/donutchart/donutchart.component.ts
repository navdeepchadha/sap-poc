import { Component, OnInit } from '@angular/core';


declare var google;

@Component({
    selector: 'donut-chart',
    template: '<div id="piechart"></div>'
})

export class DonutChartComponent implements OnInit {

    drawDonutChart() {

        var data = google.visualization.arrayToDataTable([
            ['Risk', 'Percent of Risk'],
            ['Very Low', 2],
            ['Low', 4],
            ['Medium', 6],
            ['High', 0.3],
            // ['Sleep', 7]
        ]);

        var options = {
            // title: '',
            pieHole: 0.2,
            colors: ['bbff33', '5eff33', '33b5ff', 'ff338a'],
            legend: { position: 'bottom', textStyle: { color: 'black', fontSize: 9 } },
            pieSliceTextStyle: { fontSize: 9 },

        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
    }

    ngOnInit() {
        google.charts.load('current', { 'packages': ['corechart'] });

        google.charts.setOnLoadCallback(this.drawDonutChart);


    }

}