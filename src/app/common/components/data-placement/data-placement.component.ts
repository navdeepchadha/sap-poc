import { Component, OnInit } from '@angular/core';
import { GooglemapService } from 'app/common/services/googleService/googlemap.service';


@Component({
  selector: 'app-data-placement',
  templateUrl: './data-placement.component.html',
  styleUrls: ['./data-placement.component.css']
})
export class DataPlacementComponent implements OnInit {

  constructor(private mapService:GooglemapService) { }
  alertHidden=true;
  map
  ngOnInit() {
    
    setTimeout(()=>{
      this.map = this.mapService.createMap('gmap-placement', {lat: 47.922366, lng: 1.296949} );
      
          this.mapService.customerPoint( this.map, {lat: 48.021908, lng: 3.803539} );
          
          this.mapService.customerPoint( this.map, {lat: 48.314997, lng: -3.139820} );
          
          this.mapService.dataCenterRotation( this.map, {lat: 49.712770, lng: 1.891918}, 0.8 , 0.5 );
          this.mapService.dataPath(this.map, {lat: 48.314997, lng: -3.139820}, {lat: 49.712770, lng: 1.891918} );
          setTimeout(()=>{this.alertHidden=false},5000); 
              
    },
    3000);
    
  }

  
}
