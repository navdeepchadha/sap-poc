import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataPlacementComponent } from './data-placement.component';
import { GooglemapService } from "app/common/services/googleService/googlemap.service";

describe('DataPlacementComponent', () => {
  let component: DataPlacementComponent;
  let fixture: ComponentFixture<DataPlacementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataPlacementComponent ],
      providers: [ GooglemapService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataPlacementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
