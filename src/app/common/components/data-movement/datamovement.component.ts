import { Component, OnInit } from '@angular/core';
import { GooglemapService } from 'app/common/services/googleService/googlemap.service';
import { Router, NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized, ActivatedRoute, Params } from '@angular/router';



@Component({
  selector: 'app-data-movement',
  templateUrl: './datamovement.component.html',
  styleUrls: ['./datamovement.component.css']
})
export class DataMovementComponent implements OnInit {
  currentUnion = 2;
  map;
  constructor(private mapService:GooglemapService, private router: Router, private activatedRoute: ActivatedRoute ) { 
    this.listenRoute();
  }

  ngOnInit() {
    this.timeOutFun();
  }


listenRoute(){
  this.router.events.forEach((event) => {
    if(event instanceof NavigationEnd) {
      console.log( this.activatedRoute.snapshot['_urlSegment']['segments'][1]['path'], event, this.activatedRoute )
      this.currentUnion = this.activatedRoute.snapshot['_urlSegment']['segments'][1]['path'];
      this.timeOutFun();
    }

  });
}

timeOutFun(){
  setTimeout(()=>{
    if(this.currentUnion == 1){
      this.loadEuropeUnion();
    }else  if(this.currentUnion == 2){
     this.loadSwisUnion();
    }
   else{
    this.loadAmericaUnion();
   }
    
  },3000);

}

loadEuropeUnion(){
      this.map = this.mapService.createMap('gmap', {lat:  52.033604, lng:23.964335}  );
  
      // this.mapService.customerPoint( this.map, {lat: 51.033604, lng:23.964335} );
      
      this.mapService.customerPoint( this.map, {lat: 52.033604, lng: 20} );
      this.mapService.customerPoint( this.map, {lat: 59.033604, lng: 27} );
      
      this.mapService.dataCenter( this.map, {lat:  56.033604, lng:10}, 2.5 , 1. );
      
      this.mapService.dataCenter( this.map, {lat: 56.033604, lng: 25}, 0.5 , 0.3 );
      
      // this.mapService.dataPath(this.map, {lat: 48.021908, lng: 3.803539}, {lat: 44.699965, lng: 2.116646} );
      // this.mapService.dataPath(this.map,  {lat: 44.699965, lng: 2.116646}, {lat: 48.314997, lng: -3.139820});
      // this.mapService.dataPath(this.map, {lat: 49.712770, lng: 1.891918}, {lat: 44.699965, lng: 2.116646} );
      // this.mapService.dataPath(this.map, {lat: 56.033604, lng: 25}, {lat:59.033604, lng:27} );
      
      this.mapService.dataFlow(this.map, {lat: 59.033604, lng: 27}, {lat: 56.033604, lng: 25} );
      // this.mapService.dataFlow(this.map, {lat: 44.699965, lng: 2.116646}, {lat: 48.314997, lng: -3.139820});
      // this.mapService.dataFlow(this.map, {lat: 49.712770, lng: 1.891918}, {lat: 44.699965, lng: 2.116646} );
      // this.mapService.dataFlow(this.map, {lat: 48.314997, lng: -3.139820}, {lat: 49.712770, lng: 2.116646} );
}
loadAmericaUnion(){
  this.map = this.mapService.createMap('gmap', {lat: 60.652855, lng:-105.386922}  );
  this.mapService.customerPoint( this.map, {lat: 60.652855, lng:-105.386922} );
  this.mapService.customerPoint( this.map, {lat:  64.652855, lng: -100.386922} );
  this.mapService.customerPoint( this.map, {lat:  69.652855, lng: -115.386922} );
  this.mapService.dataCenter( this.map, {lat: 55.652855, lng: -95.386922},4 , 1.3 );
  
  
  // this.mapService.dataCenter( this.map, {lat:  56.033604, lng:10}, 2.5 , 1. );
  // this.mapService.dataCenter( this.map, {lat: 56.033604, lng: 25}, 0.5 , 0.3 );
  // this.mapService.dataPath(this.map, {lat: 48.021908, lng: 3.803539}, {lat: 44.699965, lng: 2.116646} );
  // this.mapService.dataPath(this.map,  {lat: 44.699965, lng: 2.116646}, {lat: 48.314997, lng: -3.139820});
  // this.mapService.dataPath(this.map, {lat: 49.712770, lng: 1.891918}, {lat: 44.699965, lng: 2.116646} );
  // this.mapService.dataPath(this.map, {lat: 56.033604, lng: 25}, {lat:59.033604, lng:27} );
  // this.mapService.dataFlow(this.map, {lat: 59.033604, lng: 27}, {lat: 56.033604, lng: 25} );
  // this.mapService.dataFlow(this.map, {lat: 44.699965, lng: 2.116646}, {lat: 48.314997, lng: -3.139820});
  // this.mapService.dataFlow(this.map, {lat: 49.712770, lng: 1.891918}, {lat: 44.699965, lng: 2.116646} );
  // this.mapService.dataFlow(this.map, {lat: 48.314997, lng: -3.139820}, {lat: 49.712770, lng: 2.116646} );
}


loadSwisUnion(){
  this.map = this.mapService.createMap('gmap', {lat: 42.922366, lng: 7.296949} );
  this.mapService.customerPoint( this.map, {lat: 48.021908, lng: 3.803539} );
  this.mapService.customerPoint( this.map, {lat: 48.314997, lng: -3.139820} );
  this.mapService.dataCenter( this.map, {lat: 49.712770, lng: 1.891918}, 0.8 , 0.5 );
  this.mapService.dataCenter( this.map, {lat: 44.699965, lng: 2.116646}, 2.5 , 0.3 );
  this.mapService.dataPath(this.map, {lat: 48.021908, lng: 3.803539}, {lat: 44.699965, lng: 2.116646} );
  this.mapService.dataPath(this.map,  {lat: 44.699965, lng: 2.116646}, {lat: 48.314997, lng: -3.139820});
  this.mapService.dataPath(this.map, {lat: 49.712770, lng: 1.891918}, {lat: 44.699965, lng: 2.116646} );
  this.mapService.dataPath(this.map, {lat: 48.314997, lng: -3.139820}, {lat: 49.712770, lng: 2.116646} );
  this.mapService.dataFlow(this.map, {lat: 48.021908, lng: 3.803539}, {lat: 44.699965, lng: 2.116646} );
  this.mapService.dataFlow(this.map, {lat: 44.699965, lng: 2.116646}, {lat: 48.314997, lng: -3.139820});
  this.mapService.dataFlow(this.map, {lat: 49.712770, lng: 1.891918}, {lat: 44.699965, lng: 2.116646} );
  this.mapService.dataFlow(this.map, {lat: 48.314997, lng: -3.139820}, {lat: 49.712770, lng: 2.116646} );
}

    private newFunction() {
        ;
    }
}
