import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterModule, Routes } from '@angular/router'; 
import { DataMovementComponent } from "app/common/components/data-movement/datamovement.component";
import { GooglemapService } from "app/common/services/googleService/googlemap.service";

describe('DataMovementComponent', () => {
  let component: DataMovementComponent;
  let fixture: ComponentFixture<DataMovementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ RouterModule ],
      declarations: [ DataMovementComponent ],
      providers: [ GooglemapService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataMovementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
