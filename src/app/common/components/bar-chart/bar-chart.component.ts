import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
declare var google; 

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css']
})
export class BarChartComponent implements OnInit {

  @Input() data:any = {}  ;
  @Input() iconStatus = false;
  @Output() graphClicked = new EventEmitter();
  constructor() {

   }

  ngOnInit() {
    //this.initGoogleChart();
  }

  barClicked(barIndex,rowIndex){
    if(this.iconStatus){
      console.log(rowIndex,barIndex);
      this.data.items[rowIndex].compliance[barIndex].chaticon = true;
      this.data.items[rowIndex].compliance[barIndex].commentbox = true;
      this.graphClicked.emit();
    }
   
  }

  commentSaved(barIndex,rowIndex){
    console.log(barIndex,rowIndex);
    this.data.items[rowIndex].compliance[barIndex].commentbox = false;
  }
  iconpopup(bar){
    bar.commentbox=true;
    console.log('---------------',bar);
  }


  // trying with google chart....
  // initGoogleChart(){
  //   google.charts.load('current', {packages: ['corechart', 'bar']});
  //   google.charts.setOnLoadCallback(drawStacked);
    
  //   function drawStacked() {
  //         var data = google.visualization.arrayToDataTable([
  //           ['City', '2010 Population', '2000 Population'],
  //           ['New York City, NY', 8175000, 8008000],
  //           ['Los Angeles, CA', 3792000, 3694000],
  //           ['Chicago, IL', 2695000, 2896000],
  //           ['Houston, TX', 2099000, 1953000],
  //           ['Philadelphia, PA', 1526000, 1517000]
  //         ]);
    
  //         var options = {
  //           title: 'Population of Largest U.S. Cities',
  //           chartArea: {width: '50%'},
  //           isStacked: true,
  //           hAxis: {
  //             title: 'Total Population',
  //             minValue: 0,
  //           },
  //           vAxis: {
  //             title: 'City'
  //           },
  //           annotations: {
  //             boxStyle: {
  //               // Color of the box outline.
  //               stroke: '#888',
  //               // Thickness of the box outline.
  //               strokeWidth: 1,
  //               // x-radius of the corner curvature.
  //               rx: 10,
  //               // y-radius of the corner curvature.dddddddddddddrddd
  //               ry: 10,
  //               // Attributes for linear gradient fill.
  //               gradient: {
  //                 // Start color for gradient.
  //                 color1: '#fbf6a7',
  //                 // Finish color for gradient.
  //                 color2: '#33b679',
  //                 // Where on the boundary to start and
  //                 // end the color1/color2 gradient,
  //                 // relative to the upper left corner
  //                 // of the boundary.
  //                 x1: '0%', y1: '0%',
  //                 x2: '100%', y2: '100%',
  //                 // If true, the boundary for x1,
  //                 // y1, x2, and y2 is the box. If
  //                 // false, it's the entire chart.
  //                 useObjectBoundingBoxUnits: true
  //               }
  //             }
  //           }
  //         };
  //         var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
  //         chart.draw(data, options);
  //       }
  // }

}
