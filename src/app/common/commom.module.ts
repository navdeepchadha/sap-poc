import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DataMovementComponent } from 'app/common/components/data-movement/datamovement.component';
import { PageNotFoundComponent } from 'app/common/components/page-not-found/page-not-found.component';
import { GooglemapService } from 'app/common/services/googleService/googlemap.service';
import { DataPlacementComponent } from './components/data-placement/data-placement.component';
import { DataAccessComponent } from './components/data-access/data-access.component';
import { BarChartComponent } from 'app/common/components/bar-chart/bar-chart.component';
import { ChatCommentComponent } from 'app/common/components/chat-comment/chat-comment.component';
import { SliderComponent } from './components/slider/slider.component';
import { GeoChartComponent } from 'app/common/components/geochart/geochart.component';
import { LineChartComponent } from 'app/common/components/linechart/linechart.component';
import { ScatterChartComponent } from 'app/common/components/scatterchart/scatterchart.component';
import { DonutChartComponent } from 'app/common/components/donutchart/donutchart.component';
import { TableComponent1 } from 'app/common/components/table/table.component';



const components = [
  PageNotFoundComponent,
  DataMovementComponent,
  DataPlacementComponent,
  DataAccessComponent,
  BarChartComponent,
  ChatCommentComponent,
  GeoChartComponent,
  DonutChartComponent,
  ScatterChartComponent,
  LineChartComponent, TableComponent1

];
const services = [
  GooglemapService
];
const pipes = [

]

@NgModule({
  declarations: [
    ...components,
    SliderComponent,
  ],
  imports: [
    CommonModule,
    RouterModule, FormsModule
  ],
  exports: [
    ...components,
    ...pipes
  ],
  providers: [
    ...services
  ]
})
export class AppCommonModule { }
