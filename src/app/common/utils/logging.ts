

export class Logging {
    
    
    private enableLog = true;
    
    constructor(){

    }
    
    log(from,msg){
        if(this.enableLog){
            console.log(from.title,msg);
        } 
    }
}