import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ApiServiceService {

  constructor( private http:Http) { }
  
  get(url){ 
    return this.http.get(url).map(res=>this.convertToJSON(res));
  }

  convertToJSON(data){
    let result = { } ;
    try{
      result = data.json();
    }catch(e){
      console.log(e);
    }
    return result;
  }

}
