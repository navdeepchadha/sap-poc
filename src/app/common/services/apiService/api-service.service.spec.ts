import { TestBed, inject } from '@angular/core/testing';
import { HttpModule } from '@angular/http';

import { ApiServiceService } from './api-service.service';

describe('ApiServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiServiceService],
      imports:[HttpModule]
    });
  });

  it('should be created', inject([ApiServiceService], (service: ApiServiceService) => {
    expect(service).toBeTruthy();
  }));
});
