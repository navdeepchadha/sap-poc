import { Injectable } from '@angular/core';
import { Subject } from 'rxjs'

@Injectable()
export class AppStateService {

  private routerWidth = new Subject();
  constructor() { 

  }

  public listenToRouterWidth(){
    return this.routerWidth;
  }

  public setRouterWidth(obj){
    this.routerWidth.next(obj);
  }

}

export class RouterViewSettings{

  constructor(){

  }
  public width;
  
}
