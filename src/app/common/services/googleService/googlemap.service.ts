import { Injectable } from '@angular/core';
declare var google: any;
declare var Bezier: any;

@Injectable()
export class GooglemapService {

  constructor() {


   }

  createMap(id, lnglat){
    var map = new google.maps.Map(document.getElementById(id), {
      center: lnglat, 
      zoom:4, 
      styles: [
        {
          featureType: 'road',
          elementType: 'geometry',
          stylers: [{visibility: 'off'}]
        },
        {
          featureType: 'poi.park',
          elementType: 'geometry',
          stylers: [{visibility: 'off'}]
        },

      ]
    });
    return map;
  }

  customerPoint(map, position){
    var radius = 10000;
    var circle = new google.maps.Circle({
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 1,
      fillColor: '#FF0000',
      fillOpacity: 0.5,
      map: map,
      center: position , 
      radius: radius - 5000
    });

    // var circle1 = this.drawDotedLine(map,'#FF0000',1,1);
    // circle1.setPath( this.getCircleCircumferencePoints(position, radius , 50) );

    // var circle2 = this.drawDotedLine(map,'#FF0000',1,1);
    // circle2.setPath( this.getCircleCircumferencePoints(position, radius + 0.05 , 50) );

    // var circle3 = this.drawDotedLine(map,'#FF0000',1,1);
    // circle3.setPath( this.getCircleCircumferencePoints(position, radius + 0.1 , 50) );
    
    var circle1 =  new google.maps.Circle({
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 1,
      // fillColor: '#FF0000',
       fillOpacity: 0,
      map: map,
      center: position , 
      radius: radius
    });


    var circle2 = new google.maps.Circle({
      strokeColor: '#FF0000',
      strokeOpacity: 0.5,
      strokeWeight: 1,
      // fillColor: '#FF0000',
       fillOpacity: 0,
      map: map,
      center: position , 
      radius: radius + 5000
    });

    var circle3 = new google.maps.Circle({
      strokeColor: '#FF0000',
      strokeOpacity: 0.3,
      strokeWeight: 1,
      // fillColor: '#FF0000',
       fillOpacity: 0,
      map: map,
      center: position , 
      radius: radius + 10000
    });

    setInterval(function() {
      // circle1.setPath( this.getCircleCircumferencePoints(position, radius , 50) );
      // circle2.setPath( this.getCircleCircumferencePoints(position, radius + 0.05 , 50) );
      // circle3.setPath( this.getCircleCircumferencePoints(position, radius+ 0.1 , 50) );
      
        circle1.setRadius(radius);
        circle2.setRadius(radius + 5000);
        circle3.setRadius(radius + 10000);
        
        if(radius<15000){
          radius = radius + 500;
        }else{
          radius = 10000;
        }
        
    }, 50);
  }

  dataCenter(map, position, dataSize, criticalDataSize){
    var rectangle = [
      // top point
      {lat: position.lat + 0.1 , lng: position.lng }, 
      {lat: position.lat + 0.1 , lng: position.lng },
      // right point
      {lat: position.lat, lng: position.lng + 0.15 },
      {lat: position.lat, lng: position.lng + 0.15 },
      // bottom point
      {lat: position.lat - 0.1 , lng: position.lng },
      {lat: position.lat - 0.1, lng: position.lng },
      // left point 
      {lat: position.lat, lng: position.lng - 0.15 },
      {lat: position.lat, lng: position.lng - 0.15 },
    ];
    var dCenter = new google.maps.Polygon({
      paths: rectangle,
      strokeColor: '#28b0ae',
      strokeOpacity: 0.8,
      strokeWeight: 1,
      fillColor: '#28b0ae',
      fillOpacity: 0.8,
      zIndex: 2
    });
    dCenter.setMap(map);

    // var circle = new google.maps.Circle({
    //   strokeColor: '#00ffff',
    //   strokeOpacity: 0,
    //   strokeWeight: 1,
    //   fillColor: '#00ffff',
    //   fillOpacity: 0.3,
    //   map: map,
    //   center: position , 
    //   radius: dataSize,
    //   zIndex: 1
    // });

    let points1 = this.getCircleCircumferencePoints(position, dataSize , 50);
    this.drawFillPoints(map,points1,'#00ffff',0.3);

    let points2 = this.getCircleCircumferencePoints(position, criticalDataSize , 50);
    let line = this.drawDotedLine(map,'#e92222',1,1);
    line.setPath(points2);
  }

  dataCenterRotation(map,position,dataSize, criticalDataSize){
    var rectangle = [
      // top point
      {lat: position.lat + 0.1 , lng: position.lng }, 
      {lat: position.lat + 0.1 , lng: position.lng },
      // right point
      {lat: position.lat, lng: position.lng + 0.15 },
      {lat: position.lat, lng: position.lng + 0.15 },
      // bottom point
      {lat: position.lat - 0.1 , lng: position.lng },
      {lat: position.lat - 0.1, lng: position.lng },
      // left point 
      {lat: position.lat, lng: position.lng - 0.15 },
      {lat: position.lat, lng: position.lng - 0.15 },
    ];
    var dCenter = new google.maps.Polygon({
      paths: rectangle,
      strokeColor: '#28b0ae',
      strokeOpacity: 0.8,
      strokeWeight: 1,
      fillColor: '#28b0ae',
      fillOpacity: 0.8,
      zIndex: 2
    });
    dCenter.setMap(map);

    // var circle = new google.maps.Circle({
    //   strokeColor: '#00ffff',
    //   strokeOpacity: 0,
    //   strokeWeight: 1,
    //   fillColor: '#00ffff',
    //   fillOpacity: 0.3,
    //   map: map,
    //   center: position , 
    //   radius: dataSize,
    //   zIndex: 1
    // });

    let points1 = this.getCircleCircumferencePoints(position, dataSize , 50);
    this.drawFillPoints(map,points1,'#00ffff',0.3);

    let points2 = this.getCircleCircumferencePoints(position, criticalDataSize , 50);
    let line = this.drawDotedLine(map,'#e92222',1,1);
    setInterval(()=>{
      let temparr=points2[0];
      points2.shift();
      points2.push(temparr);
      line.setPath(points2);
    },200);
        
  }

  dataPath(map, source,destination){
    let points = this.getCurvePoints(source,destination);
    let line = this.drawDotedLine(map,'#138eb8',1,1);
    line.setPath(points);
    
  }

  dataFlow(map,source,destination){
    let points = this.getCurvePoints(source,destination);
    let line = this.darwSolidLine(map,'#138eb8',2,1)
    //line.setPath(points);
    let pathProgress = [];
    let progressIndex = 0;
    setInterval(function(){
      if(progressIndex<points.length){
        pathProgress.push(points[progressIndex]);
        progressIndex = progressIndex + 1;
      }else{
        pathProgress.shift();
        if(pathProgress.length==0){
          progressIndex = 0;
        }
      }
      line.setPath(pathProgress);
    },100)
  }

  private getCircleCircumferencePoints(center,radius,smoothness){
    // https://www.tutorialspoint.com/computer_graphics/circle_generation_algorithm.htm
    // http://www.css-resources.com/Good-JavaScript-Circle-Algorithm.html
    var point = [];
    var x,y,xmiddle = center.lat ,ymiddle = center.lng;
    var xlast = -1;
    var ylast = -1;
    var smooth = smoothness;
    //console.log(xmiddle,ymiddle)
    for (var i = 0; i <= smooth; i++) {
      x = parseFloat(xmiddle+(radius*Math.sin(i*2*(Math.PI/smooth)))+0.0)
      y = parseFloat(ymiddle+(radius*Math.cos(i*2*(Math.PI/smooth)))*1.5)
      
      if (xlast != x || ylast != y){
      xlast = x;ylast = y;
      //console.log(x,y); 
      point.push( {lat: x , lng: y } )
      }
    }
    //point.push(point[0]);
    return point;
    // point.push( {lat: center.lat + 0.1 , lng: center.lng } );
    // point.push( {lat: center.lat - 0.1 , lng: center.lng } );
    // point.push( {lat: center.lat , lng: center.lng + 0.15 } );
    // point.push( {lat: center.lat , lng: center.lng - 0.15 } );

    // var line1 = this.drawDotedLine(map)
    // line1.setPath(point);
  }

  private drawDotedLine(map, strokeColor, strokeWeight, strokeOpacity){
    var lineSymbol = {
      path: 'M 0,-1 0,1',
      strokeOpacity: strokeOpacity,
      strokeWeight: strokeWeight,
      scale: 2
    };
    var line = new google.maps.Polyline({
      //path: point,
      strokeOpacity: 0,
      strokeColor: strokeColor,
      icons: [{
        icon: lineSymbol,
        offset: '0',
        repeat: '7px'
      }],
      map: map
    });
    return line;
  }

  private darwSolidLine(map, strokeColor, strokeWeight, strokeOpacity){
    var line = new google.maps.Polyline({
      strokeOpacity: strokeOpacity,
      strokeColor: strokeColor,
      strokeWeight: strokeWeight,
      map: map
    });
    return line;
  }

  private drawFillPoints(map,points,color,opacity){
    var fill = new google.maps.Polygon({
      paths: points,
      strokeColor: color,
      strokeOpacity: 0,
      strokeWeight: 3,
      fillColor: color,
      fillOpacity: opacity,
      map: map
    });
  }

  private getCurvePoints(source,destination){
    // https://pomax.github.io/bezierjs/
    // https://github.com/Pomax/bezierjs
    
    let midx,midy;
    let xDifference = (source.lat+destination.lat)/2;
    let yDifference = (source.lng+destination.lng)/2;
    if(xDifference < yDifference){
      midx = xDifference;
      midy = destination.lng;
      
    }else{
      midx = destination.lat;
      midy = yDifference;
    }
    var curve = new Bezier(source.lat,source.lng , midx,midy , destination.lat,destination.lng);
    var LUT = curve.getLUT(16);
    //console.log(LUT);
    let points2 = [];
    for(let item of LUT){
      points2.push( {lat:item.x,lng:item.y} ); 
    }
    //console.log(points2);
    return points2;
  }

}
