import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { PageNotFoundComponent } from 'app/common/components/page-not-found/page-not-found.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { AppRoutingModule } from 'app/app.routing';
import { AppCommonModule } from 'app/common/commom.module';
import { LoginComponent } from './modules/login/login.component';
import { ReactiveFormsModule } from '@angular/forms'
import { LoginServiceService } from 'app/modules/login/login-service.service';
import { ApiServiceService } from 'app/common/services/apiService/api-service.service';
import { HttpModule } from "@angular/http";
import { AppStateService } from 'app/common/services/appState/app-state.service';
import { RiskOccurrencesModule } from 'app/modules/risk-occurrences/risk-occurrences.module';
import { VmNodeAccessModule } from 'app/modules/vm-node/vm-node-access/vm-node-access.module';
import { SupportTicketAccessModule } from 'app/modules/support-ticket-access/support-ticket-access.module';
import { MaterializeModule } from 'angular2-materialize';
// import { MaterialModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CarouselComponent } from './modules/carousel/carousel.component';
import { TableComponent } from './modules/table/table.component';
import { TableDialog } from './modules/table/tabledialog';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CarouselComponent, TableComponent, TableDialog
  ],
  imports: [
    BrowserModule, FormsModule,
    AppRoutingModule,
    AppCommonModule, CommonModule,
    ReactiveFormsModule,
    HttpModule,
    RiskOccurrencesModule,
    VmNodeAccessModule,
    SupportTicketAccessModule,
    MaterializeModule, BrowserAnimationsModule,
  ],
  providers: [LoginServiceService, ApiServiceService, AppStateService],
  bootstrap: [AppComponent]
})
export class AppModule { }

