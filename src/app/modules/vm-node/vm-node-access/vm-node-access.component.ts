import { Component, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-vm-node-access',
  templateUrl: './vm-node-access.component.html',
  styleUrls: ['./vm-node-access.component.css']
})
export class VMNodeAccessComponent {

  vmNodeAccessId;

  constructor(private route:ActivatedRoute){
    this.route.params.subscribe((params: {id: string}) => {
      this.vmNodeAccessId = params
    });
  }

}
