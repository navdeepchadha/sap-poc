import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VMNodeAccessRouting } from 'app/modules/vm-node/vm-node-access/vm-node-access.routing';
import { VMNodeAccessComponent } from 'app/modules/vm-node/vm-node-access/vm-node-access.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [VMNodeAccessComponent]
})
export class VmNodeAccessModule { }
