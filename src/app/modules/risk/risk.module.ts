import { NgModule } from '@angular/core';
import { RiskRouting } from 'app/modules/risk/risk.routing';
import { RiskComponent } from 'app/modules/risk/risk.component';
import { UnionsComponent } from './unions/unions.component';
import { CommonModule } from '@angular/common';
import { UnionsDetailsComponent } from './unions-details/unions-details.component';
import { AppCommonModule } from 'app/common/commom.module';

@NgModule({
  declarations: [
    RiskComponent,
    UnionsComponent,
    UnionsDetailsComponent
  ],
  imports: [
    RiskRouting,
    CommonModule,
    AppCommonModule
  ],
  exports: [],
  providers: [],
  bootstrap: []
})
export class RiskModule { }
