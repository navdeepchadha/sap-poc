import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-unions',
  template: '<h1>hdgcuidh</h1>',
  styleUrls: ['./unions.component.css']
})
export class UnionsComponent implements OnInit {


  dataTabs = [
    {label:'Data Access', link: 'data-access'},
    {label:'Data Movement', link: 'data-movement'},
    {label:'Data Placement', link: 'data-placement'}
  ]
  activeDataTab;
  constructor() { }

  ngOnInit() {
  }

  dataTabClicked(item){
    this.activeDataTab = item.link;
  }
  expand()
  {  
       var div = document.getElementById("expand-div");  
       if (div.style.display !== "none") 
       {  
           div.style.display = "none";  
       }  
       else
       {  
           div.style.display = "block";  
       }  
  }  
}
