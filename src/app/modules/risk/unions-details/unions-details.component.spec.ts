import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnionsDetailsComponent } from './unions-details.component';

describe('UnionsDetailsComponent', () => {
  let component: UnionsDetailsComponent;
  let fixture: ComponentFixture<UnionsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnionsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnionsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
