import { Component } from '@angular/core';
import {Router} from '@angular/router';


@Component({
    selector: 'carousel-component',
    templateUrl: './carousel.component.html',
    styleUrls: ['./carousel.component.css'],
})

export class CarouselComponent {

    check: number;

     constructor(private route:Router) { }

     gotoDashboard(){        
         let subscriptionId = localStorage.getItem("subscriptionID");
         this.route.navigate(['/dashboard', subscriptionId]);
     }
        
}