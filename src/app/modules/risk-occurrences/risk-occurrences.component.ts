import { Component, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
@Component({
  selector: 'app-risk-occurrences',
  templateUrl: './risk-occurrences.component.html',
  styleUrls: ['./risk-occurrences.component.css']
})
export class RiskOccurrencesComponent {

  riskOccurencesID;

  constructor(private route:ActivatedRoute){
    this.route.params.subscribe((params: {id: string}) => {
      this.riskOccurencesID = params
    });
  }

}
