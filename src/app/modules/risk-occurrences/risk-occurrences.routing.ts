import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RiskOccurrencesComponent } from 'app/modules/risk-occurrences/risk-occurrences.component';
import { VMNodeAccessComponent } from 'app/modules/vm-node/vm-node-access/vm-node-access.component';


const crisisCenterRoutes: Routes = [
    // {
    //   path: '',
    //   component: RiskOccurrencesComponent,
    // },
    
  ];
@NgModule({
    imports: [
      RouterModule.forChild(crisisCenterRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
      
    ]
  })
  export class RiskOccurrencesRouting { }