import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RiskOccurrencesRouting } from 'app/modules/risk-occurrences/risk-occurrences.routing';
import { RiskOccurrencesComponent } from 'app/modules/risk-occurrences/risk-occurrences.component';
import { RouterModule } from '@angular/router';




@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [RiskOccurrencesComponent]
})
export class RiskOccurrencesModule { }
