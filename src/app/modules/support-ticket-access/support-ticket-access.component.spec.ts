import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupportTicketAccessComponent } from './support-ticket-access.component';

describe('SupportTicketAccessComponent', () => {
  let component: SupportTicketAccessComponent;
  let fixture: ComponentFixture<SupportTicketAccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupportTicketAccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupportTicketAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
