import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-support-ticket-access',
  templateUrl: './support-ticket-access.component.html',
  styleUrls: ['./support-ticket-access.component.css']
})
export class SupportTicketAccessComponent implements OnInit {

  supportTicketId;

  constructor(private route:ActivatedRoute){
    this.route.params.subscribe((params: {id: string}) => {
      this.supportTicketId = params
    });
  }

  ngOnInit() {
  }

}
