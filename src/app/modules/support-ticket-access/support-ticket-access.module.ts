import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SupportTicketAccessComponent } from 'app/modules/support-ticket-access/support-ticket-access.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [SupportTicketAccessComponent]
})
export class SupportTicketAccessModule { }
