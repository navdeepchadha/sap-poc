"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var animations_1 = require("@angular/animations");
var tableData = (function () {
    function tableData() {
    }
    return tableData;
}());
exports.tableData = tableData;
exports.dataTable = [
    {
        id: 1,
        name: "Leanne Graham",
        username: "Bret",
        email: "Sincere@april.biz"
    },
    {
        id: 2,
        name: "Ervin Howell",
        username: "Antonette",
        email: "Shanna@melissa.tv"
    },
    // ... list of items
    {
        id: 11,
        name: "Nicholas DuBuque",
        username: "Nicholas.Stanton",
        email: "Rey.Padberg@rosamond.biz"
    }
];
var TableComponent = (function () {
    function TableComponent() {
        // settings = {
        //     columns: {
        //         id: {
        //             title: 'ID'
        //         },
        //         name: {
        //             title: 'Full Name'
        //         },
        //         username: {
        //             title: 'User Name'
        //         },
        //         email: {
        //             title: 'Email'
        //         }
        //     }
        // };
        this.tableValue = exports.dataTable;
        this.classSelect = false;
        this.showDialog = false;
    }
    TableComponent.prototype.onSelectTableData = function (dataLoop) {
        this.selectedTableData = dataLoop;
    };
    TableComponent.prototype.onSelectData = function () {
        this.classSelect = true;
    };
    return TableComponent;
}());
TableComponent = __decorate([
    core_1.Component({
        selector: 'table-app',
        template: "<h3>Table</h3>\n    <!--<ng2-smart-table [settings]=\"settings\" [source]=\"data\"></ng2-smart-table>-->\n    <button md-raised-button (click)=\"onSelectData()\"> Comment </button>\n    <table>\n  <tr>\n    <th>id</th>\n    <th>name</th>\n    <th>username</th>\n    <th>email</th>\n  </tr>\n  <tr *ngFor=\"let value of tableValue\" [class.changeCursor]=\"classSelect\" [class.selected]=\"value === selectedTableData\" (click)=\"showDialog\">\n    <td>{{value.id}}</td>\n    <td>{{value.name}}</td>\n    <td>{{value.username}}</td>\n    <td>{{value.email}}</td>\n  </tr>\n</table>\n<app-dialog [(visible)]=\"showDialog\">\n  <h1>Hello World</h1>\n  <button (click)=\"showDialog = !showDialog\" class=\"btn\">Close</button>\n</app-dialog>\n    ",
        styles: ["\n    table {\n    font-family: arial, sans-serif;\n    border-collapse: collapse;\n    width: 100%;\n}\n\ntd, th {\n    border: 1px solid #dddddd;\n    text-align: left;\n    padding: 8px;\n}\n\n.selected {\n      background-color: #CFD8DC !important;\n      color: white;\n    }\n    .changeCursor{\n        cursor:pointer;\n        \n    }\n    "]
    })
], TableComponent);
exports.TableComponent = TableComponent;
var DialogComponent = (function () {
    function DialogComponent() {
        this.closable = true;
        this.visibleChange = new core_1.EventEmitter();
    }
    DialogComponent.prototype.ngOnInit = function () { };
    DialogComponent.prototype.close = function () {
        this.visible = false;
        this.visibleChange.emit(this.visible);
    };
    return DialogComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], DialogComponent.prototype, "closable", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], DialogComponent.prototype, "visible", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], DialogComponent.prototype, "visibleChange", void 0);
DialogComponent = __decorate([
    core_1.Component({
        selector: 'app-dialog',
        template: "<div [@dialog] *ngIf=\"visible\" class=\"dialog\">\n  <ng-content></ng-content>\n  <button *ngIf=\"closable\" (click)=\"close()\" aria-label=\"Close\" class=\"dialog__close-btn\">X</button>\n</div>\n<div *ngIf=\"visible\" class=\"overlay\" (click)=\"close()\"></div>",
        styles: [".overlay {\n  position: fixed;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  background-color: rgba(0, 0, 0, 0.5);\n  z-index: 999;\n}\n\n.dialog {\n  z-index: 1000;\n  position: fixed;\n  right: 0;\n  left: 0;\n  top: 20px;\n  margin-right: auto;\n  margin-left: auto;\n  min-height: 200px;\n  width: 90%;\n  max-width: 520px;\n  background-color: #fff;\n  padding: 12px;\n  box-shadow: 0 7px 8px -4px rgba(0, 0, 0, 0.2), 0 13px 19px 2px rgba(0, 0, 0, 0.14), 0 5px 24px 4px rgba(0, 0, 0, 0.12);\n}\n\n@media (min-width: 768px) {\n  .dialog {\n    top: 40px;\n  }\n}\n\n.dialog__close-btn {\n  border: 0;\n  background: none;\n  color: #2d2d2d;\n  position: absolute;\n  top: 8px;\n  right: 8px;\n  font-size: 1.2em;\n}"],
        animations: [
            animations_1.trigger('dialog', [
                animations_1.transition('void => *', [
                    animations_1.style({ transform: 'scale3d(.3, .3, .3)' }),
                    animations_1.animate(100)
                ]),
                animations_1.transition('* => void', [
                    animations_1.animate(100, animations_1.style({ transform: 'scale3d(.0, .0, .0)' }))
                ])
            ])
        ]
    }),
    __metadata("design:paramtypes", [])
], DialogComponent);
exports.DialogComponent = DialogComponent;
//# sourceMappingURL=table.component.js.map