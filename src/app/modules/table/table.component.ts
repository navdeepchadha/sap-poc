import { Component, Input, OnInit } from '@angular/core';
// import { MdDialog,MdDialogConfig  } from '@angular/material';
import { TableDialog } from './tabledialog';
// import { AppDataService, SetDataService, SetData } from './app-data.service';
import { ActivatedRoute } from '@angular/router';


export class tableData {
    BusinessUnit: string;
    Issuecategory: string;
    BuisnessImpact: string;
    ComplianceIssue: number;
    ComplianceRate: number;
    ShowCommentIcon: boolean;
    ShowComment: string;
}

export const dataTable: tableData[] = [
    {
        BusinessUnit: 'Authentication',
        Issuecategory: "Financial Risks",
        BuisnessImpact: "High",
        ComplianceIssue: 16,
        ComplianceRate: 92.6,
        ShowCommentIcon: false,
        ShowComment: ''
    },
    {
        BusinessUnit: 'Reporting',
        Issuecategory: "Financial Risks",
        BuisnessImpact: "High",
        ComplianceIssue: 37,
        ComplianceRate: 89.2,
        ShowCommentIcon: false,
        ShowComment: ''
    },

    // ... list of items

    {
        BusinessUnit: 'Vendor Master File Changes',
        Issuecategory: "Financial Risks",
        BuisnessImpact: "High",
        ComplianceIssue: 22,
        ComplianceRate: 87.3,
        ShowCommentIcon: false,
        ShowComment: ''
    },
    {
        BusinessUnit: 'Authentication',
        Issuecategory: "Financial Risks",
        BuisnessImpact: "Medium",
        ComplianceIssue: 7,
        ComplianceRate: 57.3,
        ShowCommentIcon: false,
        ShowComment: ''
    },
    {
        BusinessUnit: 'Master File Changes',
        Issuecategory: "Financial Risks",
        BuisnessImpact: "Low",
        ComplianceIssue: 14,
        ComplianceRate: 37.3,
        ShowCommentIcon: false,
        ShowComment: ''
    }
];

@Component({
    selector: 'table-app',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.css']

})

export class TableComponent implements OnInit {
    // constructor(public setdataservice: SetDataService, public dialog: MdDialog) {

    // }
    tableValue = dataTable;
    selectedTableData: tableData;

    leaveComment: string;
    // showcomment: boolean = true;
    // commentAdded: string;
    popupVisible: boolean = true;
    commentindex: number;
    commentPopup(indexvalue) {
        this.leaveComment = this.tableValue[indexvalue].ShowComment;
        this.popupVisible = false;
        this.commentindex = indexvalue;
    }
       //chart with comment
       //================================
       commentClicked = false;
       comment(){
         this.commentClicked = true;
       }
       barChartObj = { 
        title: 'Compliance by Business Unit',
        items: [
          {label:'Finance', compliance:[{year:2016,percentage:25,color:'lightblue',chaticon:false},{year:2017,percentage:40,color:'grey',chaticon:false},] },
          {label:'HR', compliance:[{year:2016,percentage:35,color:'lightblue',chaticon:false},{year:2017,percentage:25,color:'grey',chaticon:false},]  },
          {label:'Production', compliance:[{year:2016,percentage:20,color:'lightblue',chaticon:false},{year:2017,percentage:50,color:'grey',chaticon:false},]  },
          {label:'R&D', compliance:[{year:2016,percentage:30,color:'lightblue',chaticon:false},{year:2017,percentage:50,color:'grey ',chaticon:false},]  }
        ]
      };
      
      
    
     //=========================================
       graphClicked(){
         this.commentClicked = false;
       }
        

    saveComment(comment, valueIndex) {

        // this.commentAdded = comment;
        this.tableValue[valueIndex].ShowCommentIcon = true;
        this.tableValue[valueIndex].ShowComment = comment;
        this.leaveComment = "";
        this.popupVisible = true;
        // this.showcomment = false;
    }

    newComment(value) {
        this.leaveComment = value;
        this.popupVisible = false;

    }

    close() {
        this.leaveComment = "";
        this.popupVisible = true;
    }


    cursorchange: boolean;
    // commentgetdata: SetData[];
    datacmmt;


    ngOnInit(): void {
        // this.getAppData();
    }



    classSelect = false;
    activeClass = false;


    onSelectData(): void {
        // this.getAppData();
        if (!this.datacmmt) {
            this.classSelect = true;
            this.cursorchange = false;
        }
        else {
            this.cursorchange = this.datacmmt.activeComment;
            this.classSelect = false;
            console.log(this.datacmmt)
        }


    }



}
