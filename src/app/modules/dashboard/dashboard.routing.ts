import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from 'app/modules/dashboard/dashboard.component';



export const dashboardRoutes: Routes = [
    {
      path: ':subscriptionID',
      component: DashboardComponent
      
    }
  ];
@NgModule({
    imports: [
      RouterModule.forChild(dashboardRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
      
    ]
  })
  export class DashBoardRouting { }