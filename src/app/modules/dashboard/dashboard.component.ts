import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'

declare var google;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {
  
  gotoTablePage() {
    this.router.navigate(['/table']);
  }

  subscriptionID;
  chartTypeData = [
    { subscriptionID: 1100, chart: 'line' },
    { subscriptionID: 1101, chart: 'bar' },
    { subscriptionID: 1102, chart: 'column' },
  ];
  chartType;
  constructor(private route: ActivatedRoute, private router: Router) {
    this.subscriptionID = this.route.snapshot.params['subscriptionID']
    for (let item of this.chartTypeData) {
      if (item.subscriptionID == this.subscriptionID) {
        this.chartType = item.chart;
      }
    }
  }

  valuecolor: number;
  //Donut Chart Starts
  drawDonutChart() {

    var data = google.visualization.arrayToDataTable([
      ['Risk', 'Percent of Risk'],
      ['Very Low', 2],
      ['Low', 4],
      ['Medium', 6],
      ['High', 0.3],
      // ['Sleep', 7]
    ]);

    var options = {
      // title: '',
      pieHole: 0.2,
      colors: ['bbff33', '5eff33', '33b5ff', 'ff338a'],
      legend: { position: 'bottom', textStyle: { color: 'black', fontSize: 9 } },
      pieSliceTextStyle: { fontSize: 9 },

    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart'));

    chart.draw(data, options);
  }
  //Donut Chart Ends

  //Scatter Chart starts
  drawScatterChart() {

    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Time');
    data.addColumn('number', 'Low');
    data.addColumn('number', 'High');

    data.addRows([
      // ['8pm', 0.00],
      [0, 20, 67], [1, 17, 88], [2, 2, 77],
      [3, 5, 93], [4, 4, 85], [5, 35, 91],
      [6, 15, 71], [7, 27, 78], [8, 8, 93],
      [9, 9, 80], [10, 10, 82], [11, 10, 75],
      [12, 17, 80], [13, 3, 90], [14, 21, 72],
      // ['6am', 50.00]
    ]);

    var options = {
      // title: 'Age vs. Weight comparison',
      // hAxis: { title: 'Age', minValue: 0, maxValue: 15 },
      vAxis: { title: 'Risks' },
      legend: { position: 'bottom', textStyle: { color: 'black', fontSize: 9 } },
      colors: ['#087037', 'red'],

    };

    var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));

    chart.draw(data, options);
  }
  //Scatter Chart Ends

  //Trend Chart Starts
  // drawTrendChart() {
  //   var data = google.visualization.arrayToDataTable([
  //     ['Generation', 'Descendants', 'Year'],
  //     [0, 1, 100], [1, 33, 49], [2, 269, 65], [3, 200, 150]
  //   ]);

  //   var options = {
  //     // title: 'Descendants by Generation',
  //     hAxis: { title: 'Generation' },
  //     vAxis: { title: 'Descendants' },
  //     trendlines: {
  //       0: {
  //         type: 'exponential',
  //         visibleInLegend: true,
  //       }
  //     }
  //   };

  //   var chart = new google.visualization.ScatterChart(document.getElementById('polynomial2_div'));
  //   chart.draw(data, options);
  // }
  //Trend Chart Ends  

  //Geo Chart Starts



  drawRegionsMap(s) {

    var dataRows = [
      ['Country', 'Risk'],
      ['Germany', 36], ['Poland', 8], ['France', 6], ['Italy', 24], ['Netherlands', 6], ['United Kingdom', 24]

    ];


    // extract column index 1 for color values
    var colorValues = dataRows.map(function (val) {
      return val.slice(-1);
    });
    // remove column label
    colorValues.splice(0, 1);
    // sort ascending
    colorValues.sort(function (a: any, b: any) { return a - b });

    // build color names red <= -10 > yellow <= 10 > green
    var colorNames = [];

    colorValues.forEach((value: any) => {
      // for (var i = 0; i < colorValues.length; i++) {
      if (value <= 15) {
        colorNames.push('#6CC417');
      } else if ((value > 15) && (value <= 30)) {
        colorNames.push('#3090C7');
      } else {
        colorNames.push('#DC381F');
      }
    });

    var data = google.visualization.arrayToDataTable(dataRows);
    var options = {
      region: '150', // Europe
      colorAxis: {
        colors: colorNames,
        values: colorValues
      },
      backgroundColor: '#81d4fa',
      datalessRegionColor: '#ffffff',
      defaultColor: '#f5f5f5',
      legend: 'none',
      magnifyingGlass: { enable: true, zoomFactor: 7.5 },
      keepAspectRatio: true

    };




    var geoChart = new google.visualization.GeoChart(document.getElementById('geochart-colors'));
    geoChart.draw(data, options);
    google.visualization.events.addListener(geoChart, 'select', (obj) => {
      var selectedItem = geoChart.getSelection();
      //console.log(selectedItem);
      s.mapclicked(selectedItem);
    });



  };


  //Geo Chart Ends  

  //Line Chart Starts
  // drawLineChart() {

  //   var presentDate = new Date();
  //   var currentYear = presentDate.getFullYear();


  //   var data = new google.visualization.DataTable();
  //   data.addColumn('date', 'Month');
  //   data.addColumn('number', 'Current');
  //   data.addColumn('number', 'Projected');

  //   data.addRows([
  //     [new Date(currentYear, 0), 0, 0], [new Date(currentYear, 1), 1, 10], [new Date(currentYear, 2), 2, 23],
  //     [new Date(currentYear, 3), 3, 17], [new Date(currentYear, 4), 4, 18], [new Date(currentYear, 5), 5, 9],
  //     [new Date(currentYear, 6), 6, 11], [new Date(currentYear, 7), 10.3, 16.6],
  //     [new Date(currentYear, 8), 7.4, 13.3],
  //     [new Date(currentYear, 9), 4.4, 9.9],
  //     [new Date(currentYear, 10), 1.1, 6.6],
  //     [new Date(currentYear, 11), 2, 4.5]
  //   ]);


  //   var formatter = new google.visualization.NumberFormat({
  //     fractionDigits: 2,
  //     prefix: 'R$:'
  //   });
  //   formatter.format(data, 1);
  //   var dateFormatter = new google.visualization.NumberFormat({
  //     pattern: 'MMM yyyy'
  //   });
  //   dateFormatter.format(data, 0);

  //   var options = {
  //     hAxis: {
  //       // title: 'example title',
  //       slantedText: false,
  //       slantedTextAngle: 45,
  //       textStyle: {
  //         fontSize: 10
  //       },
  //       format: 'MMM'
  //     },
  //     vAxis: {
  //       // title: 'Popularity'
  //     },
  //     series: {
  //       0: { color: '#3e708b', pointsVisible: true },
  //       1: { curveType: 'function', pointsVisible: true, lineDashStyle: [4, 4] }
  //     },
  //     legend: { position: 'bottom', textStyle: { color: 'black', fontSize: 9 } }
  //   };

  //   var chart = new google.visualization.LineChart(document.getElementById('Linechart_div'));

  //   chart.draw(data, options);
  // }
  //Line Chart Ends


  ngOnInit() {

    google.charts.load('current', { 'packages': ['corechart'] });
    google.charts.load('current', { 'packages': ['line'] });
    // google.charts.setOnLoadCallback(this.drawLineChart);

    google.charts.load('current', {
      'packages': ['geochart'],
      // Note: you will need to get a mapsApiKey for your project.
      // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
      'mapsApiKey': 'AIzaSyBa9Aj01s0c08PJC2jtGOSGeyJSSMJNbno'
    });
    google.charts.setOnLoadCallback(() => this.drawRegionsMap(this));

    google.charts.setOnLoadCallback(this.drawDonutChart);

    google.charts.setOnLoadCallback(this.drawScatterChart);
    // google.charts.setOnLoadCallback(this.drawTrendChart);


    // var map = new google.maps.Map(document.getElementById('map'), {
    //   zoom: 5,
    //   center: { lat: 54.5260, lng: 15.2551 },
    // });

  }


  /* pieChartData = {
    chartType: 'PieChart',
    dataTable: [
      ['Task', 'Hours per Day'],
      ['Work', 11],
      ['Eat', 2],
      ['Commute', 2],
      ['Watch TV', 2],
      ['Sleep', 7]
    ],
    options: { 'title': 'Tasks' },
  }; */

  mapclicked(obj) {
    let unionId = obj[0].row;
    console.log(obj)
    this.router.navigate(['/risk-by-union', unionId, 'data-movement']);
  }

}


