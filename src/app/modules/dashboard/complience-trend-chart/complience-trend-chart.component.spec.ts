import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComplienceTrendChartComponent } from './complience-trend-chart.component';
import { ApiServiceService } from "app/common/services/apiService/api-service.service";
import { HttpModule } from '@angular/http';
describe('ComplienceTrendChartComponent', () => {
  let component: ComplienceTrendChartComponent;
  let fixture: ComponentFixture<ComplienceTrendChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComplienceTrendChartComponent ],
      providers:[ApiServiceService],
      imports:[HttpModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplienceTrendChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
