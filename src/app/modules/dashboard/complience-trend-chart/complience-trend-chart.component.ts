import { Component, OnInit, ViewEncapsulation, Input, OnChanges } from '@angular/core';
import { ApiServiceService } from 'app/common/services/apiService/api-service.service';
import { apiEndPoints } from 'app/config';

declare var google;

@Component({
  selector: 'app-complience-trend-chart',
  templateUrl: './complience-trend-chart.component.html',
  styleUrls: ['./complience-trend-chart.component.css'],
  encapsulation: ViewEncapsulation.None

})

export class ComplienceTrendChartComponent implements OnInit, OnChanges {

  @Input() chartType;

  constructor(private apiService:ApiServiceService) { }

  ngOnInit() { }

  ngOnChanges(){
    console.log(this.chartType);

    if(this.chartType == 'line'){
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(()=>this.lineChart(this));
    }
    if(this.chartType == 'bar'){
      google.charts.load('current', {packages: ['corechart', 'bar']});
      google.charts.setOnLoadCallback(this.barChart);
    }
    if(this.chartType == 'column'){
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(this.columnChart);
    }
    
  }

  lineChart(s) {
    // var data = google.visualization.arrayToDataTable([
    //   ['Year', 'Sales', 'Expenses'],
    //   ['2004',  1000,      400],
    //   ['2005',  1170,      460],
    //   ['2006',  660,       1120],
    //   ['2007',  1030,      540]
    // ]);
    let url = apiEndPoints.dashboard.complianceTrendChart.lineChartData;
    s.apiService.get(url).subscribe( (data) => {
      console.log(data);
    
      let d = [
        ['Year', 'Sales', 'Expenses'],
        ['2004',  1000,      400],
        ['2005',  1170,      460],
        ['2006',  660,       1120],
        ['2007',  1030,      540]
      ];

      var data = google.visualization.arrayToDataTable(d);

      var options = {
        title: 'Company Performance',
        curveType: 'function',
        legend: { position: 'bottom' }
      };

      var chart = new google.visualization.LineChart(document.getElementById('chartdiv'));

      chart.draw(data, options);

    });
  }

  barChart() {
    var data = google.visualization.arrayToDataTable([
      ['City', '2010 Population', '2000 Population'],
      ['New York City, NY', 8175000, 8008000],
      ['Los Angeles, CA', 3792000, 3694000],
      ['Chicago, IL', 2695000, 2896000],
      ['Houston, TX', 2099000, 1953000],
      ['Philadelphia, PA', 1526000, 1517000]
    ]);

    var options = {
      title: 'Population of Largest U.S. Cities',
      chartArea: {width: '50%'},
      hAxis: {
        title: 'Total Population',
        minValue: 0,
        textStyle: {
          bold: true,
          fontSize: 12,
          color: '#4d4d4d'
        },
        titleTextStyle: {
          bold: true,
          fontSize: 18,
          color: '#4d4d4d'
        }
      },
      vAxis: {
        title: 'City',
        textStyle: {
          fontSize: 14,
          bold: true,
          color: '#848484'
        },
        titleTextStyle: {
          fontSize: 14,
          bold: true,
          color: '#848484'
        }
      }
    };
    var chart = new google.visualization.BarChart(document.getElementById('chartdiv'));
    chart.draw(data, options);
  }

  columnChart() {
    var data = google.visualization.arrayToDataTable([
      ['Year', 'Sales', 'Expenses', 'Profit'],
      ['2014', 1000, 400, 200],
      ['2015', 1170, 460, 250],
      ['2016', 660, 1120, 300],
      ['2017', 1030, 540, 350]
    ]);

    var options = {
      chart: {
        title: 'Company Performance',
        subtitle: 'Sales, Expenses, and Profit: 2014-2017',
      }
    };

    var chart = new google.charts.Bar(document.getElementById('chartdiv'));

    chart.draw(data, google.charts.Bar.convertOptions(options));
  }

}
