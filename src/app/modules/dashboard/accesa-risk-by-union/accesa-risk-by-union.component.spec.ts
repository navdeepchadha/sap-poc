import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccesaRiskByUnionComponent } from './accesa-risk-by-union.component';

describe('AccesaRiskByUnionComponent', () => {
  let component: AccesaRiskByUnionComponent;
  let fixture: ComponentFixture<AccesaRiskByUnionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccesaRiskByUnionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccesaRiskByUnionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
