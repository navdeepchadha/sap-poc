import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrequentContactListComponent } from './frequent-contact-list.component';

describe('FrequentContactListComponent', () => {
  let component: FrequentContactListComponent;
  let fixture: ComponentFixture<FrequentContactListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrequentContactListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrequentContactListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
