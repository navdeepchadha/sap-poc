import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashBoardRouting } from 'app/modules/dashboard/dashboard.routing';
import { ComplienceTrendChartComponent } from './complience-trend-chart/complience-trend-chart.component';
import { MyTaskComponent } from './my-task/my-task.component';
import { AccesaRiskByUnionComponent } from './accesa-risk-by-union/accesa-risk-by-union.component';
import { RiskOccurenceComponent } from './risk-occurence/risk-occurence.component';
import { NewsFeedComponent } from './news-feed/news-feed.component';
import { AccessRiskVoilationComponent } from './access-risk-voilation/access-risk-voilation.component';
import { FrequentContactListComponent } from './frequent-contact-list/frequent-contact-list.component';

@NgModule({
  imports: [
    CommonModule,DashBoardRouting
  ],
  declarations: [DashboardComponent, ComplienceTrendChartComponent, MyTaskComponent, AccesaRiskByUnionComponent, RiskOccurenceComponent, NewsFeedComponent, AccessRiskVoilationComponent, FrequentContactListComponent]
})
export class DashboardModule { }
