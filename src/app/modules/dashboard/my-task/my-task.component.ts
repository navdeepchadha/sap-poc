import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'


export class MyTasks {

  tasks: string;
  comments: string;
}

const TASK: MyTasks[] = [
  { tasks: 'Check Completed Support Ticket', comments: 'Quick followup & Verify' },
  { tasks: 'Monthly Report for Lena', comments: '' },
  { tasks: 'Cloud Security Online Training', comments: 'TBD' },
  { tasks: 'Prepare Report for Upcoming Audit', comments: 'TBD' },
  { tasks: 'Data Migration', comments: ' From Rome to Italy' },
  { tasks: 'UK patch release', comments: '' },
  { tasks: 'Server Patch update', comments: '27 Sep 2017' },
  { tasks: 'Data Migration', comments: '' },
  { tasks: 'Data Migration from London', comments: '' }
];

@Component({
  selector: 'app-my-task',
  templateUrl: './my-task.component.html',
  styleUrls: ['./my-task.component.css']
})
export class MyTaskComponent implements OnInit {

  task = TASK;

  showTasks = this.task.length - 4;
  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }

  goToCarousel() {
    this.router.navigate(['/carousel']);

  }

}
