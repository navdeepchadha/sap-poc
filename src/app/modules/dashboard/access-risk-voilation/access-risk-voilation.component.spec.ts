import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessRiskVoilationComponent } from './access-risk-voilation.component';

describe('AccessRiskVoilationComponent', () => {
  let component: AccessRiskVoilationComponent;
  let fixture: ComponentFixture<AccessRiskVoilationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessRiskVoilationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessRiskVoilationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
