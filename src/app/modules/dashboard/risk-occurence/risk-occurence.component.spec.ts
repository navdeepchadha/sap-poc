import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskOccurenceComponent } from './risk-occurence.component';

describe('RiskOccurenceComponent', () => {
  let component: RiskOccurenceComponent;
  let fixture: ComponentFixture<RiskOccurenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskOccurenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskOccurenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
