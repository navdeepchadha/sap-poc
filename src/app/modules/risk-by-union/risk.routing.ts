import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RiskComponent } from 'app/modules/risk-by-union/risk.component';
import { UnionsComponent } from 'app/modules/risk-by-union/unions/unions.component';
import { UnionsDetailsComponent } from 'app/modules/risk-by-union/unions-details/unions-details.component';



const crisisCenterRoutes: Routes = [
    {
      path: '',
      component: RiskComponent,
      children: [
        {
            path: ':unionId',
            component: UnionsComponent,
            children: [
                { path: ':dataTabId', component: UnionsDetailsComponent }
              ]
        }
      ]
    }
  ];
@NgModule({
    imports: [
      RouterModule.forChild(crisisCenterRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
      
    ]
  })
  export class RiskRouting { }