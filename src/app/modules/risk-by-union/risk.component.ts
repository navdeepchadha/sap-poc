import { Component, Inject } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-risk',
  templateUrl: './risk.component.html',
  styleUrls: ['./risk.component.css']
})
export class RiskComponent {

  constructor(private route:Router){

  }

  toDashboard():any{
    let route1 = this.route;
    let subscriptionID=localStorage.getItem("subscriptionID");
        route1.navigate(['/dashboard',subscriptionID]);
  }
}
