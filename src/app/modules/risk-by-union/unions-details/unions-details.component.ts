import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-unions-details',
  templateUrl: './unions-details.component.html',
  styleUrls: ['./unions-details.component.css']
})
export class UnionsDetailsComponent implements OnInit {

  activePage;
  constructor(private route: ActivatedRoute) { 
    this.activePage = this.route.snapshot.params["dataTabId"];
    this.route.params.subscribe( (params)=>{
      this.activePage = params["dataTabId"];
    })
    
  }

  ngOnInit() {
  }

  barChartObj = { 
    title: 'Compliance by Business Unit',
    items: [
      {label:'Finance', compliance:[{year:2016,percentage:25,color:'lightblue',chaticon:false},{year:2017,percentage:40,color:'grey',chaticon:false},] },
      {label:'HR', compliance:[{year:2016,percentage:35,color:'lightblue',chaticon:false},{year:2017,percentage:25,color:'grey',chaticon:false},]  },
      {label:'Production', compliance:[{year:2016,percentage:20,color:'lightblue',chaticon:false},{year:2017,percentage:50,color:'grey',chaticon:false},]  },
      {label:'R&D', compliance:[{year:2016,percentage:30,color:'lightblue',chaticon:false},{year:2017,percentage:50,color:'grey ',chaticon:false},]  }
    ]
  };

  commentClicked = false;
  comment(){
    this.commentClicked = true;
  }

  graphClicked(){
    this.commentClicked = false;
  }
  
}
