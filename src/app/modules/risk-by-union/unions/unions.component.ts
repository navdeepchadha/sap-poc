import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-unions',
  templateUrl: './unions.component.html',
  styleUrls: ['./unions.component.css']
})
export class UnionsComponent implements OnInit {

  unionTabs = [
    {label:'Europe Union', link: '1'},
    {label:'Swiss-German Union', link: '2'},
    {label:'North-America Union', link: '3'}
  ]
  activeUnionTab = '2';

  unionTabClicked(item){
    this.activeUnionTab = item.link;
  }

  dataTabs = [
    {label:'Data Access', link: 'data-access'},
    {label:'Data Movement', link: 'data-movement'},
    {label:'Data Placement', link: 'data-placement'}
  ]
  activeDataTab = 'data-movement';
  constructor() { }

  ngOnInit() {
  }

  dataTabClicked(item){
    this.activeDataTab = item.link;
  }
  expand()
  {  
       var div = document.getElementById("expand-div");  
       if (div.style.display !== "none") 
       {  
           div.style.display = "none";  
       }  
       else
       {  
           div.style.display = "block";  
       }  
  }  
}
